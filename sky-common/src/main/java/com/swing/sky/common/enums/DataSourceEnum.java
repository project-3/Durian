package com.swing.sky.common.enums;

/**
 * 数据源
 *
 * @author swing
 */
public enum DataSourceEnum {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
